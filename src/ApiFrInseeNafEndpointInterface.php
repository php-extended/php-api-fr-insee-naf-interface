<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

use Iterator;
use Stringable;

/**
 * ApiFrInseeNafEndpointInterface interface file.
 * 
 * This interface represents an unique endpoint to get NAP, NAFv1, NAFv2 and
 * NAFA data.
 * 
 * @author Anastaszor
 */
interface ApiFrInseeNafEndpointInterface extends Stringable
{
	
	/**
	 * Gets an iterator over all the nap lv1 sections.
	 * 
	 * @return Iterator<integer, ApiFrInseeNap1973Lv1SectionInterface>
	 */
	public function getNap1973Lv1SectionIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv2 divisions.
	 *
	 * @return Iterator<integer, ApiFrInseeNap1973Lv2DivisionInterface>
	 */
	public function getNap1973Lv2DivisionIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv3 groups.
	 *
	 * @return Iterator<integer, ApiFrInseeNap1973Lv3GroupInterface>
	 */
	public function getNap1973Lv3GroupIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv4 classes.
	 *
	 * @return Iterator<integer, ApiFrInseeNap1973Lv4ClassInterface>
	 */
	public function getNap1973Lv4ClassIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv1 sections.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf1993Lv1SectionInterface>
	 */
	public function getNaf1993Lv1SectionIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv2 divisions.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf1993Lv2DivisionInterface>
	 */
	public function getNaf1993Lv2DivisionIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv3 groups.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf1993Lv3GroupInterface>
	 */
	public function getNaf1993Lv3GroupIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv4 classes.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf1993Lv4ClassInterface>
	 */
	public function getNaf1993Lv4ClassIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv4 subclasses.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf1993Lv5SubclassInterface>
	 */
	public function getNaf1993Lv5SubclassIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv1 sections.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2003Lv1SectionInterface>
	 */
	public function getNaf2003Lv1SectionIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv2 divisions.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2003Lv2DivisionInterface>
	 */
	public function getNaf2003Lv2DivisionIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv3 groups.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2003Lv3GroupInterface>
	 */
	public function getNaf2003Lv3GroupIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv4 classes.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2003Lv4ClassInterface>
	 */
	public function getNaf2003Lv4ClassIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv5 subclasses.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2003Lv5SubclassInterface>
	 */
	public function getNaf2003Lv5SubclassIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv1 sections.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2008Lv1SectionInterface>
	 */
	public function getNaf2008Lv1SectionIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv2 divisions.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2008Lv2DivisionInterface>
	 */
	public function getNaf2008Lv2DivisionIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv3 groups.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2008Lv3GroupInterface>
	 */
	public function getNaf2008Lv3GroupIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv4 classes.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2008Lv4ClassInterface>
	 */
	public function getNaf2008Lv4ClassIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv5 subclasses.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2008Lv5SubclassInterface>
	 */
	public function getNaf2008Lv5SubclassIterator() : Iterator;
	
	/**
	 * Gets an iterator over all the nap lv6 artisanat.
	 *
	 * @return Iterator<integer, ApiFrInseeNaf2008Lv6ArtisanatInterface>
	 */
	public function getNaf2008Lv6ArtisanatIterator() : Iterator;
	
}
