<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

use Stringable;

/**
 * ApiFrInseeNaf2003Lv5SubclassInterface interface file.
 * 
 * This defines the lv5 of the 2003 norm of principal activities.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeNaf2003Lv5SubclassInterface extends Stringable
{
	
	/**
	 * Gets the id of this subclass.
	 * 
	 * @return string
	 */
	public function getIdNaf2003Lv5Subclass() : string;
	
	/**
	 * Gets the id of the related class.
	 * 
	 * @return string
	 */
	public function getIdNaf2003Lv4Class() : string;
	
	/**
	 * Gets the id of the related next subclass.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv5Subclass() : string;
	
	/**
	 * Gets the libelle of this subclass.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
