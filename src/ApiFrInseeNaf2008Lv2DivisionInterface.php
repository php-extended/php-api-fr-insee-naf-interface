<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

use Stringable;

/**
 * ApiFrInseeNaf2008Lv2DivisionInterface interface file.
 * 
 * This defines the lv2 of the 2008 norm of principal activities.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeNaf2008Lv2DivisionInterface extends Stringable
{
	
	/**
	 * Gets the id of this division.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv2Division() : string;
	
	/**
	 * Gets the id of the related section.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv1Section() : string;
	
	/**
	 * Gets the libelle, reduced to 40 chars max.
	 * 
	 * @return string
	 */
	public function getLibelle40() : string;
	
	/**
	 * Gets the libelle, reduced to 65 chars max.
	 * 
	 * @return string
	 */
	public function getLibelle65() : string;
	
	/**
	 * Gets the libelle of this division.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
