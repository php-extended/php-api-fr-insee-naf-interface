<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

use Stringable;

/**
 * ApiFrInseeNap1973Lv1SectionInterface interface file.
 * 
 * This defines the lv1 of the 1973 norm of principal activities.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeNap1973Lv1SectionInterface extends Stringable
{
	
	/**
	 * Gets the id of this section.
	 * 
	 * @return string
	 */
	public function getIdNap1973Lv1Section() : string;
	
	/**
	 * Gets the libelle of this section.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
