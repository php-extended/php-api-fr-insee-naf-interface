<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

use Stringable;

/**
 * ApiFrInseeNaf1993Lv3GroupInterface interface file.
 * 
 * This defines the lv3 of the 1993 norm of principal activities.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeNaf1993Lv3GroupInterface extends Stringable
{
	
	/**
	 * Gets the id of this group.
	 * 
	 * @return string
	 */
	public function getIdNaf1993Lv3Group() : string;
	
	/**
	 * Gets the id of the related division.
	 * 
	 * @return string
	 */
	public function getIdNaf1993Lv2Division() : string;
	
	/**
	 * Gets the libelle of this group.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
