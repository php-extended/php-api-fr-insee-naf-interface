<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-naf-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeNaf;

use Stringable;

/**
 * ApiFrInseeNaf2008Lv6ArtisanatInterface interface file.
 * 
 * This defines the lv6 of the 2008 norm of principal activities.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeNaf2008Lv6ArtisanatInterface extends Stringable
{
	
	/**
	 * Gets the id of this artisanat.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv6Artisanat() : string;
	
	/**
	 * Gets the id of the related subclass.
	 * 
	 * @return string
	 */
	public function getIdNaf2008Lv5Subclass() : string;
	
	/**
	 * Gets the libelle of this artisanat.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
